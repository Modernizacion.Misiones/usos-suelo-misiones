///////////////////////////////////////////////////////////////////////////////////
// Clasificación Supervisada Uso del Suelo en Misiones Algoritmo con Random Forest
// Paso 1 - Generación de la imagen
///////////////////////////////////////////////////////////////////////////////////

/*
Autores:
- Carlos Brys
- Andrés Leszczuk
- Instituto Geográfico Nacional (https://github.com/ign-argentina/mapa-nacional-cobertura-suelo/)
- Subsecretar`ìa de Ordenamiento Territorial. Ministerio de Ecologia y RNR
Proyecto: Clasificacion de coberturas para el mape de uso del suelo en Misiones 
Período: segundo semestre del año 2023
Fecha de actualización: 11/03/2024
*/


// Importación de Activos
var sar = ee.ImageCollection("COPERNICUS/S1_GRD"), // Imagenes Sentinel SAR 1-GRD 
    ContornoMisiones = ee.FeatureCollection("projects/mapas-de-misiones/assets/Misiones-shp"),
    DEM = ee.Image("USGS/SRTMGL1_003") // DEM del repositorio Google Earth Engine con una resolución de 30m 
;


// Visualización de la colección en color natural

var bandas = ['B4_mean','B3_mean','B2_mean'];
var color_natural = {bands: bandas, min: 0, max: 5000};

var bandas2 = ['B4','B3','B2'];
var color_natural2 = {bands: bandas2, min: 0, max: 5000};


//////////////////// Coleccion de imagenes ///////////////////

// SENTINEL 2A/B - Nivel de preprocesamiento 1C

var s2c_01 = ee.ImageCollection("COPERNICUS/S2")
.filterDate('2023-06-01', '2023-12-31') 
.filterBounds(ContornoMisiones) 
.filterMetadata('CLOUDY_PIXEL_PERCENTAGE', 'less_than', 0.3); // Seleccionar % de Nubes

// SENTINEL 1 - Imagenes Radar banda C

var sar_vh = ee.ImageCollection(sar) // Banda doble de polarización cruzada, transmisión vertical/recepción horizontal 
.filterDate('2023-06-01', '2023-12-31')
.filterBounds(ContornoMisiones)
.filter(ee.Filter.eq('instrumentMode', 'IW'))
.filter(ee.Filter.eq('orbitProperties_pass', 'DESCENDING'))
.select('VH').mean()
.rename('VH_mean')

var sar_vv = ee.ImageCollection(sar) // Banda de polarización, tranmisión vertical/recepción vertical
.filterDate('2023-06-01', '2023-12-31')
.filterBounds(ContornoMisiones)
.filter(ee.Filter.eq('instrumentMode', 'IW'))
.filter(ee.Filter.eq('orbitProperties_pass', 'DESCENDING'))
.select('VV').mean()
.rename('VV_mean')

/////////////////// Indices Espectrales ////////////////////

// Se reducen los valores de la colección de SENTINEL por los valores estadísticos: media, min y max

// Media

var s2c_2 = s2c_01.reduce(ee.Reducer.mean()).clip(ContornoMisiones);

// Se agrega la imagene en colección natural al mapa
Map.addLayer(s2c_2, color_natural, 'Color Natural'); 

// Minima

var s2c_min = s2c_01.reduce(ee.Reducer.min())

// Máxima

var s2c_max = s2c_01.reduce(ee.Reducer.max())

// Se cálculan los índices a partir de los valores anteriores 

// Normalized Difference Vegetation Index (NDVI) = (B8 - B4) / (B8 + B4)

var NDVI= s2c_2.normalizedDifference(['B8_mean','B4_mean']).rename('NDVImean')

var NDVImin = s2c_min.normalizedDifference(['B8_min', 'B8_min']).rename('NDVImin')

var NDVImax = s2c_max.normalizedDifference(['B8_max', 'B8_max']).rename('NDVImax')

var NDVIrango = NDVImax.subtract(NDVImin).rename('NDVIrango')

// Normalized Difference Water Index (NDWI) = (B8 - B3) / (B8 + B3)

var NDWI= s2c_2.normalizedDifference(['B3_mean','B8_mean']).rename('NDWImean')

var NDWImin = s2c_min.normalizedDifference(['B3_min','B8_min']).rename('NDWImin')

var NDWImax = s2c_max.normalizedDifference(['B3_max','B8_max']).rename('NDWImax')

var NDWIrango = NDWImax.subtract(NDWImin).rename('NDWIrango')

//Bare Soil Index (BSI) = (B11 + B4) – (B8 + B2) / (B11 + B4) + (B8 + B2)

// Media

var T1_1= s2c_2.select('B11_mean');
var T1_2= s2c_2.select('B4_mean');
var T1_3= s2c_2.select('B8_mean');
var T1_4=s2c_2.select('B2_mean');
var b11addb4=T1_1.add(T1_2);
var b8addb2=T1_3.add(T1_4);
var termino1=b11addb4.subtract(b8addb2);
var termino2= b11addb4.add(b8addb2);
var BSI= termino1.divide(termino2).rename("BSImean");

// Min

var T2_1= s2c_min.select('B11_min');
var T2_2= s2c_min.select('B4_min');
var T2_3= s2c_min.select('B8_min');
var T2_4=s2c_min.select('B2_min');
var b11addb4_2=T2_1.add(T2_2);
var b8addb2_2=T2_3.add(T2_4);
var termino1_2=b11addb4_2.subtract(b8addb2_2);
var termino2_2= b11addb4_2.add(b8addb2_2);

var BSImin= termino1_2.divide(termino2_2).rename("BSImin");

// Max

var T3_1= s2c_max.select('B11_max');
var T3_2= s2c_max.select('B4_max');
var T3_3= s2c_max.select('B8_max');
var T3_4= s2c_max.select('B2_max');
var b11addb4_3=T3_1.add(T3_2);
var b8addb2_3=T3_3.add(T3_4);
var termino1_3=b11addb4_3.subtract(b8addb2_3);
var termino2_3= b11addb4_3.add(b8addb2_3);

var BSImax= termino1_3.divide(termino2_3).rename("BSImax");

//Rango

var BSIrango = BSImax.subtract(BSImin).rename('BSIrango')

// Enhanced Vegetation Index (EVI2)

var EVI = s2c_2.expression(
  '2.4*((NIR-RED)/(NIR+RED+1))',{
    'NIR':s2c_2.select('B8_mean'),
    'RED':s2c_2.select('B4_mean'),
}).rename('EVI2mean'); 
  
// Min  

var EVImin = s2c_min.expression(
  '2.4*((NIR-RED)/(NIR+RED+1))',{
    'NIR':s2c_min.select('B8_min'),
    'RED':s2c_min.select('B4_min'),
}).rename('EVI2min');
  
// Max

var EVImax = s2c_max.expression(
  '2.4*((NIR-RED)/(NIR+RED+1))',{
    'NIR':s2c_max.select('B8_max'),
    'RED':s2c_max.select('B4_max'),
}).rename('EVI2max')

//Rango

var EVIrango = EVImax.subtract(EVImin).rename('EVI2rango')

// Digital Elevation Model (DEM)

var DEM = DEM.rename('DEM').clip(ContornoMisiones);

// Bandas del visible e Infrarrojo

var visibleinf = s2c_2.select(['B2_mean', 'B3_mean','B4_mean', 'B8_mean', 'B11_mean']);

// Normalized Difference Snow Index (NDSI) = (Green-SWIR) / (Green+SWIR)

var ndsi= s2c_2.expression(
  '(GREEN - SWIR ) / (GREEN  + SWIR )',{
    'GREEN':s2c_2.select('B3_mean'),
    'SWIR':s2c_2.select('B11_mean'),
}).rename('NDSImean');
  
// Min

var NDSImin= s2c_min.expression(
  '(GREEN - SWIR ) / (GREEN  + SWIR )',{
    'GREEN':s2c_min.select('B3_min'),
    'SWIR':s2c_min.select('B11_min'),
}).rename('NDSImin');
  
// Max

var NDSImax= s2c_max.expression(
  '(GREEN - SWIR ) / (GREEN  + SWIR )',{
    'GREEN':s2c_max.select('B3_max'),
    'SWIR':s2c_max.select('B11_max'),
}).rename('NDSImax');

// Rango

var NDSIrango = NDSImax.subtract(NDSImin).rename('NDSIrango')

// Soil Adjusment Vegatation Index (SAVI) = [ (NIR - red ) / (NIR + red + L) ] ×(I+L)

// Media

var savi= s2c_2.expression(
  'NIR-RED/(NIR+RED+0.5)*(1+0.5)',{
    'NIR':s2c_2.select('B8_mean'),
    'RED':s2c_2.select('B4_mean'),
}).rename('SAVImean'); 
  
// Min

var SAVImin= s2c_min.expression(
  'NIR-RED/(NIR+RED+0.5)*(1+0.5)',{
    'NIR':s2c_min.select('B8_min'),
    'RED':s2c_min.select('B4_min'),
}).rename('SAVImin'); 

// Max

var SAVImax= s2c_max.expression(
  'NIR-RED/(NIR+RED+0.5)*(1+0.5)',{
    'NIR':s2c_max.select('B8_max'),
    'RED':s2c_max.select('B4_max'),
}).rename('SAVImax'); 
  
// Rango

var SAVIrango = SAVImax.subtract(SAVImin).rename('SAVIrango')

// Normalized Difference Moisture Index (NDMI) = (B08 - B11) / (B08 + B11)

// Media

var NDMImean = s2c_2.normalizedDifference(['B8_mean','B11_mean']).rename('NDMImean')

// Min

var NDMImin = s2c_min.normalizedDifference(['B8_min', 'B11_min']).rename('NDMImin')

// Max

var NDMImax = s2c_max.normalizedDifference(['B8_max', 'B11_max']).rename('NDMImax')

// Rango

var NDMIrango = NDMImax.subtract(NDMImin).rename('NDMIrango')

// Stack para la clasificación
// Se crea una imagen única con todas las bandas espectrales, 
// los índices calculados y el DEM (36 capas)

var ImagenCompuesta = ee.Image(NDVI)
.addBands(NDVImin)
.addBands(NDVImax)
.addBands(NDVIrango)
.addBands(NDWI)
.addBands(NDWImin)
.addBands(NDWImax)
.addBands(NDWIrango)
.addBands(BSI)
.addBands(BSImin)
.addBands(BSImax)
.addBands(BSIrango)
.addBands(visibleinf)
.addBands(sar_vh)
.addBands(DEM)
.addBands(sar_vv)
.addBands(ndsi)
.addBands(NDSImin)
.addBands(NDSImax)
.addBands(NDSIrango)
.addBands(EVI)
.addBands(EVImin)
.addBands(EVImax)
.addBands(EVIrango)
.addBands(savi)
.addBands(SAVImin)
.addBands(SAVImax)
.addBands(SAVIrango)
.addBands(NDMImean)
.addBands(NDMImin)
.addBands(NDMImax)
.addBands(NDMIrango)


/// Exportación ///

// carpeta donde va a exportar la imagen
var exportPath = 'projects/mapas-de-misiones/assets/';

Export.image.toAsset({
  image: ImagenCompuesta,
  assetId: exportPath + 'Imagen-de-Trabajo-Misiones',
  region: ContornoMisiones,
  description: 'Imagen-de-Trabajo-Misiones',
  scale: 30,
  maxPixels: 1e10
});


