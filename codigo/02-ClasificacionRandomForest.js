///////////////////////////////////////////////////////////////////////////////////
// Clasificación Supervisada Uso del Suelo en Misiones Algoritmo con Random Forest
// Paso 2 - Clasificaci´ón del uso del suelo usando Random Forest
///////////////////////////////////////////////////////////////////////////////////

/*
Autores:
- Carlos Brys
- Andrés Leszczuk
- Instituto Geográfico Nacional (https://github.com/ign-argentina/mapa-nacional-cobertura-suelo/)
Proyecto: Clasificacion de coberturas para el mapeo de uso del suelo en Misiones 
Período: segundo semestre del año 2023
Fecha de actualización: 15/03/2024
*/


// Importación de Activos
var ImagenTrabajo = ee.Image("projects/mapas-de-misiones/assets/Imagen-de-Trabajo-Misiones"),
    ContornoMisiones = ee.FeatureCollection("projects/mapas-de-misiones/assets/Misiones-shp"),
    areasUrbanas = ee.FeatureCollection('projects/mapas-de-misiones/assets/areas-urbanas-shp'),
    muestras_suelo = ee.FeatureCollection("projects/mapas-de-misiones/assets/usos_del_suelo_misiones") // http://u.osmfr.org/m/1035437/
;

var bandas = ['B4_mean','B3_mean','B2_mean'];
var color_natural = {bands: bandas, min: 0, max: 5000};

var bandas2 = ['B4','B3','B2'];
var color_natural2 = {bands: bandas2, min: 0, max: 5000};

// Bandas a utilizar en la clasificación
var bandas_sel = ['NDVImean', 'NDVImin', 'NDVImax', 'NDVIrango', 'NDWImean', 'NDWImin', 'NDWImax', 'NDWIrango', 'BSImean', 'BSImin', 'BSImax', 'BSIrango', 'EVI2mean', 'EVI2min', 'EVI2max', 'EVI2rango', 'DEM', 'B2_mean', 'B3_mean', 'B4_mean', 'B8_mean', 'B11_mean', 'VH_mean', 'VV_mean', 'NDSImean', 'NDSImin', 'NDSImax', 'NDSIrango', 'SAVImean', 'SAVImin', 'SAVImax', 'SAVIrango', 'NDMImean', 'NDMImin', 'NDMImax', 'NDMIrango']                              


// Extrae la imagen resultante
var imagenClasificar = ImagenTrabajo.visualize({bands: bandas_sel, min: 0, max: 0.3});

//Visualizacion de la imagen de trabajo sin las areas urbanas
//Map.addLayer(ImagenTrabajo, {}, 'Imagen a Clasificar');
//Map.centerObject(ImagenTrabajo, 8)


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// Clasificación RANDOM FOREST //////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Muestras (Incorporar shape desde "Assets" o tomar las muestras directamente desde GEE)
var muestras = muestras_suelo.filterBounds(ContornoMisiones) 
var muestras_1 = muestras.randomColumn('random') // A esas muestras se le asigna una columna aleatoria para luego poder dividir en 70/30

Export.table.toDrive(muestras_1, 'Muestras_Exportación')

// Division de muestras
// se separa un 70% de las muestras para realizar la clasificación 
// y un 30% para la validación del clasificador

var MuestrasEntrenamiento = muestras_1.filter(ee.Filter.lt('random', 0.7))
var MuestrasValidacion = muestras_1.filter(ee.Filter.gt('random', 0.3))

Export.table.toDrive(MuestrasEntrenamiento, 'Descarga_Entrenamiento')
Export.table.toDrive(MuestrasValidacion, 'Descarga_Validacion')

var Entrenamiento = ImagenTrabajo.sampleRegions({  
  collection:MuestrasEntrenamiento,
  properties:['class', 'random'],
  scale: 30
})

// Bandas a utilizar en la clasificación

var bandas_sel = ['NDVImean', 'NDVImin', 'NDVImax', 'NDVIrango', 'NDWImean', 'NDWImin', 'NDWImax', 'NDWIrango', 'BSImean', 'BSImin', 'BSImax', 'BSIrango', 'EVI2mean', 'EVI2min', 'EVI2max', 'EVI2rango', 'DEM', 'B2_mean', 'B3_mean', 'B4_mean', 'B8_mean', 'B11_mean', 'VH_mean', 'VV_mean', 'NDSImean', 'NDSImin', 'NDSImax', 'NDSIrango', 'SAVImean', 'SAVImin', 'SAVImax', 'SAVIrango', 'NDMImean', 'NDMImin', 'NDMImax', 'NDMIrango']                              

// Entreno al Clasificador RF

// El n° indica la candidad de árboles que se van a generar 
// y tiene implicancia en la velocidad de procesamiento
var classifier = ee.Classifier.smileRandomForest(500)          ///   300
.train({
  features: Entrenamiento,
  classProperty: 'class',
  inputProperties: bandas_sel
})

// Clasifico la imagen

var AreasClasificadas = ImagenTrabajo.classify(classifier)

/// Estilos ///
// Color de las Categorías
var PaletaColores = [
  'e0ff00',		//	1 Pastizal
  '829308',		//	2 Matorral
  '556c54',		//	3 Bosque
  '006400',		//	4 Coníferas
  '07edd1',		//	5 Eucalipto
  'FFFFFF',		//	6 Tabaco
  '5fed07',		//	7 Té
  '6B8E23',		//	8 Yerba mate
  '61682f',		//	9 Araucaria
  'FFFFFF',		//	10 Maíz
  'FFFFFF',		//	11 Caña_de_azúcar
  'FFFFFF',		//	12 Mandioca
  'FFFFFF',		//	13 Soja
  'FFFFFF',		//	14 Banano
  'FFFFFF',		//	15 Ananá/Piña
  'FFFFFF',		//	16 Bambú
  '9B9B9B',		//	17 Suelo_al_descubierto
  '00BFFF',		//	18 Agua
  'b3c346',    //	19 Hierba/Pasto/Pradera/Potrero
  '00FFFF',    //	20 Humedal
  'BDB76B',    // 21 Kiri
//  '000000',    //	22 Zonas urbanas
  ];

// Denominación de las categorías
var NombresClases = [
  ' 1 Pastizal',
  ' 2 Matorral',
  ' 3 Bosque',
  ' 4 Coníferas',
  ' 5 Eucalipto',
  ' 6 Tabaco',
  ' 7 Té',
  ' 8 Yerba Mate',
  ' 9 Araucaria',
  '10 Maíz',
  '11 Caña de Azúcar',
  '12 Mandioca',
  '13 Soja',
  '14 Banano',
  '15 Ananá',
  '16 Bambú',
  '17 Tierra',
  '18 Agua',
  '19 Pradera',
  '20 Humedal',
  '21 Kiri',
//  '22 Zonas Urbanas',
];
/*
var NombresClases = [
  ' 1 Grassland',
  ' 2 Scrubland',
  ' 3 Forest',
  ' 4 Conifers',
  ' 5 Eucalyptus',
  ' 6 Tobacco',
  ' 7 Tea',
  ' 8 Yerba Mate',
  ' 9 Araucaria',
  ' 10 Corn',
  '11 Sugar Cane',
  '12 Cassava',
  '13 Soybeans',
  '14 Banana',
  '15 Pineapple',
  '16 Bamboo',
  '17 Soil',
  '18 Water',
  '19 Meadow',
  '20 Wetland',
  '21 Kiri',
//  '22 Zonas Urbanas',
];
*/



// Agrego el resultado al mapa con la paleta de colores definida en Estilos (linea 23)
Map.addLayer(AreasClasificadas, {min:1, max: NombresClases.length, palette: PaletaColores}, 'Clasificación RF') 


// Matriz de Confusion y Precision

var test = AreasClasificadas.sampleRegions({
  collection: MuestrasValidacion,
  properties: ['class'],
  scale: 30
})
  
var testConfusionMatrix = test.errorMatrix('class', 'classification')

print('Matrix de confusión' , testConfusionMatrix);
print('Precisión RF', testConfusionMatrix.accuracy());
print('Coeficiente Kappa', testConfusionMatrix.kappa())

//////////////  Leyendas para el Mapa  /////////////////////////////////////////

var legend = ui.Panel({style: {position: 'bottom-left',padding: '8px 15px'}});
var legendTitle = ui.Label({value: 'Category', style: {fontWeight: 'bold',fontSize: '18px',margin: '0 0 4px 0',padding: '0'}
});
legend.add(legendTitle);
var makeRow = function(color, name) {
var colorBox = ui.Label({style: {backgroundColor: '#' + color,padding: '8px',margin: '0 0 4px 0'}
});

var description = ui.Label({value: name,style: {margin: '0 0 4px 6px'}});
return ui.Panel({widgets: [colorBox, description],layout: ui.Panel.Layout.Flow('horizontal')});
};


for (var i = 0; i < NombresClases.length; i++) {legend.add(makeRow(PaletaColores[i], NombresClases[i]));}  

Map.add(legend);


///////////////////////////////////// Exportación ///////////////////////////////////////////////////

// *** Acá hay que cortar de la imagen AreasClasificadas 
// los polígonos del shape areasUrbanas
// y guardar otre imagen que tenga los recortes

/* 
var exportPath = 'projects/mapas-de-misiones/assets/';
 
Export.image.toAsset({
  image: AreasClasificadas,
  assetId: exportPath + 'Clasificacion-Uso-Suelo-Misiones',
  region: ContornoMisiones,
  description: 'Clasificacion-Uso-Suelo-Misiones',
  scale: 30,
  maxPixels: 1e10
});


Export.image.toDrive({
  image: AreasClasificadas,
  description: 'Clasificacion_Uso_Suelo_Misiones_2023',
  folder: 'Sentinel-2',
  scale: 30, 
  region: ContornoMisiones,
  maxPixels: 1e10 // 872093662, // Si la exportación da error por n° de px, cambiar por el número que indica en esta línea. 
});



*/