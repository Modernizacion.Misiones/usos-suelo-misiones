3///////////////////////////////////////////////////////////////////////////////////
// Clasificación Supervisada Uso del Suelo en Misiones Algoritmo con Random Forest
///////////////////////////////////////////////////////////////////////////////////
 
/*
Autores:
- Carlos Brys
- Andrés Leszczuk
- Instituto Geográfico Nacional (https://github.com/ign-argentina/mapa-nacional-cobertura-suelo/)
Proyecto: Clasificacion de coberturas para el mape de uso del suelo en Misiones 
Período: segundo semestre del año 2023
Fecha de actualización: 15/03/2024
*/


/////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcion para calcular superficie por cada uno de las coberturas por cada uno de los municipios


// Importación de Activos
var ImagenTrabajo = ee.Image("projects/mapas-de-misiones/assets/Clasificacion-Uso-Suelo-Misiones"),
    ContornoMisiones = ee.FeatureCollection("projects/mapas-de-misiones/assets/Misiones-shp"),
    limitesMunicipales = ee.FeatureCollection("projects/mapas-de-misiones/assets/Municipios_de_Misiones_shp")
;

//var limitesMunicipales = ee.FeatureCollection("projects/ee-leszczukandresalejandro/assets/municipios_mnes")
//print(limitesMunicipales);

/// Estilos ///
// Color de las Categorías
var PaletaColores = [
  'e0ff00',		//	1 Pastizal
  '829308',		//	2 Matorral
  '556c54',		//	3 Bosque
  '006400',		//	4 Coníferas
  '07edd1',		//	5 Eucalipto
  'FFFFFF',		//	6 Tabaco
  '5fed07',		//	7 Té
  '6B8E23',		//	8 Yerba mate
  '61682f',		//	9 Araucaria
  'FFFFFF',		//	10 Maíz
  'FFFFFF',		//	11 Caña_de_azúcar
  'FFFFFF',		//	12 Mandioca
  'FFFFFF',		//	13 Soja
  'FFFFFF',		//	14 Banano
  'FFFFFF',		//	15 Ananá/Piña
  'FFFFFF',		//	16 Bambú
  '9B9B9B',		//	17 Suelo_al_descubierto
  '00BFFF',		//	18 Agua
  'b3c346',    //	19 Hierba/Pasto/Pradera/Potrero
  '00FFFF',    //	20 Humedal
  'BDB76B',    // 21 Kiri
//  '000000',    //	22 Zonas urbanas
  ];

// Denominación de las categorías
var NombresClases = [
  ' 1 Pastizal',
  ' 2 Matorral',
  ' 3 Bosque',
  ' 4 Coníferas',
  ' 5 Eucalipto',
  ' 6 Tabaco',
  ' 7 Té',
  ' 8 Yerba Mate',
  ' 9 Araucaria',
  '10 Maíz',
  '11 Caña de Azúcar',
  '12 Mandioca',
  '13 Soja',
  '14 Banano',
  '15 Ananá',
  '16 Bambú',
  '17 Tierra',
  '18 Agua',
  '19 Pradera',
  '20 Humedal',
  '21 Kiri',
//  '22 Zonas Urbanas',
];


// Filtro de mayoria (majority) en función a una ventana movil de moda
var ImagenTrabajo_filtrada =  ImagenTrabajo.reduceNeighborhood({
  reducer:ee.Reducer.mode(),
  kernel: ee.Kernel.square(5)});

Map.addLayer(ImagenTrabajo_filtrada, 
{min:1, max: 20, palette: PaletaColores}, 
'Clasificación RF filtrada') 

// Poligonos 
var classes = ImagenTrabajo_filtrada.reduceToVectors({
  reducer: ee.Reducer.countEvery(), 
  geometry: ContornoMisiones, 
  scale: 30,
  maxPixels: 1e10,
  tileScale : 4
});
// Agregar al mapa la clasificacion transformada en poligonos
Map.addLayer(classes,{}, 'Poligonos')


var calculateClassArea = function(feature) {
    var areas = ee.Image.pixelArea().addBands(ImagenTrabajo)
    .reduceRegion({
      reducer: ee.Reducer.sum().group({
      groupField: 1,
      groupName: 'class',
    }),
    geometry: feature.geometry(),
    scale: 10,
    maxPixels: 1e10
    })
 
    var classAreas = ee.List(areas.get('groups'))
    var classAreaLists = classAreas.map(function(item) {
      var areaDict = ee.Dictionary(item)
      var classNumber = ee.Number(
        areaDict.get('class')).format()
      var area = ee.Number(
        areaDict.get('sum')).divide(1e6).round()
      return ee.List([classNumber, area])
    })
 
    var result = ee.Dictionary(classAreaLists.flatten())
    var NombreMunicipio = feature.get('name')
    return ee.Feature(
      feature.geometry(),
      result.set('Municipio', NombreMunicipio))
}
 
var AreasMunicipios = limitesMunicipales.map(calculateClassArea);

var classes = ee.List.sequence(1, 22)
var outputFields = ee.List(
    ['Municipio']).cat(classes).getInfo()
 
Export.table.toDrive({
    collection: AreasMunicipios,
    description: 'areas_de_las_clases_por_municipio',
    folder: 'Sentinel-2',
    fileNamePrefix: 'areas_de_las_clases_por_municipio',
    fileFormat: 'CSV',
    selectors: outputFields
    })
