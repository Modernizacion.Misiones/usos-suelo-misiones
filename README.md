# Mapa de Cobertura del Suelo de la Provincia de Misiones

## 📚 Participantes

 **Dirección de Modernización de la Gestión y Gobierno Electrónico**<br>
 **Subsecretaría de Ordenamiento Territorial**<br>
 **Subsecretaría de Desarrollo Vegetal**<br>
 **Facultad de Ciencias Económicas. Universidadad Nacional de Misiones**<br>
 **Facultad de Ciencias Forestales. Universidadad Nacional de Misiones**<br>

 Autores:

 Carlos Brys, FCE-UNaM

 Andrés Leszczuk, FCF-UNaM


Este proyecto está basado en el mapa de cobertura del uso del suelo elaborado por el Instituto Geográfico Nacional (IGN): https://github.com/ign-argentina/mapa-nacional-cobertura-suelo/

Se representa la cobertura del suelo para la provincia de Misiones usando el algoritmo Random Forest

La clasificación se realizó teniendo en cuenta 20 categorías.

**Categorías:**
  1  Pastizal, 
  2  Matorral,
  3  Bosque,
  4  Coníferas,
  5  Eucalipto,
  6  Tabaco,
  7  Té,
  8  Yerba Mate,
  10 Maíz,
  11 Caña de Azúcar,
  12 Mandioca,
  13 Soja,
  14 Banano,
  15 Ananá,
  16 Bambú,
  17 Tierra,
  18 Agua,
  19 Pradera,
  20 Humedal,
  
Los puntos de control se cargaron usando la plataforma uMap  http://u.osmfr.org/m/1035437/

## ⚙️ Instrucciones



## Enlaces 🔗

> [**Enlace al repositorio en Google Earth Engine**](https://code.earthengine.google.com/?accept_repo=users/CarlosBrys/UsoSueloMisiones)

